package com.example.practicasensor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button Sensores= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Sensores=(Button)findViewById(R.id.btnSensores);
        Sensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent cambiar = new Intent(getApplicationContext(),ActividadSensorAcelerometro.class);
                startActivity(cambiar);
            }
        });
    }
}